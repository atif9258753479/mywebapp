FROM ubuntu:latest
RUN apt-get update -y
RUN apt install apache2 -y
ENV DEBIAN_FRONTEND=non-interactive
CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]
EXPOSE 80
WORKDIR /var/www/html/
VOLUME /var/apache2/log/
RUN rm -rf /var/www/html/*
ADD myapp.tar.gz /var/www/html/
RUN mv 2136_kool_form_pack/* /var/www/html/
 



